QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = datetime
TEMPLATE = app
DEFINES += QT_DEPRECATED_WARNINGS
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x050000

SOURCES += main.cpp datetime.cpp \
    clockface.cpp
HEADERS += datetime.h \
    clockface.h \
    version.h
FORMS += datetime.ui

RESOURCES += \
    images.qrc

TRANSLATIONS += translations/datetime_ca.ts \
                translations/datetime_de.ts \
                translations/datetime_el.ts \
                translations/datetime_es.ts \
                translations/datetime_fr.ts \
                translations/datetime_it.ts \
                translations/datetime_ja.ts \
                translations/datetime_nl.ts \
                translations/datetime_ro.ts \
                translations/datetime_sv.ts
